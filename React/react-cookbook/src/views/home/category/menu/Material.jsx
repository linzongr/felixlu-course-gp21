import React, { Component } from 'react'
import Menu from '@/components/menu/Menu'
import {
  MenuWrap
} from './StyledMenu'

export default class Category extends Component {
  state = {
    curTab: '肉类'
  }

  handleChangeCurTab = tab => {
    return () => {
      this.setState({
        curTab: tab
      })
    }
  }
  
  render() {
    return (
      <MenuWrap>
        <Menu 
          {...this.props} 
          {...this.state}
          onChangeCurTab={this.handleChangeCurTab}
        ></Menu>
      </MenuWrap>
    )
  }
}