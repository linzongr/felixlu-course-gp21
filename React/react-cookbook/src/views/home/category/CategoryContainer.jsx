import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import CategoryUi from './CategoryUi'
import { httpContext } from '@/context/httpContext'
import BScroll from 'better-scroll'

@withRouter
class Category extends Component {
  state = {
    type: 'category',
    categoryData: null
  }

  static contextType = httpContext

  handleTabClick = (type) => {
    let { history, match } = this.props
    return () => {
      this.setState({
        type
      })
      history.push(`${match.url}/${type}`)
    }
  }

  render() {
    return (
      <CategoryUi 
        {...this.props}
        {...this.state}
        onTabClick={this.handleTabClick}
      ></CategoryUi>
    )
  }

  async componentDidMount() {
    let categoryData = (await this.context.http.get({
      url: '/api/category'
    })).data

    this.setState({
      categoryData
    })

    //menu scroll
    new BScroll('.menu-tab', {
      click: true
    })
    new BScroll('.menu-body', {
      click: true
    })

    // 防刷新,tab复位
    let urlArr = this.props.location.pathname.split('/')
    let type = urlArr[urlArr.length-1]
    this.setState({
      type
    })
  }
}

export default Category