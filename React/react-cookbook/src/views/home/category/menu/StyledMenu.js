import styled from 'styled-components'

const MenuWrap = styled.div `
  overflow: hidden;
  flex: 1;
`

export {
  MenuWrap
}