import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Search from '@/components/search/Search'
import Category from './menu/Category'
import Material from './menu/Material'
import {
  CategoryWrap,
  NavUl
} from './StyledCategory'

export default function CategoryUi(props) {
  let url = props.match.url
  return (
    <CategoryWrap>
      <nav>
        <NavUl
          color="#fff"
          width="1px"
          radius={0.15}
        >
          <li
            onClick={props.onTabClick('category')}
            className={props.type === 'category' ? 'active' : ''}
          >分类</li>
          <li
            onClick={props.onTabClick('material')}
            className={props.type === 'material' ? 'active' : ''}
          >食材</li>
          <li
            className={props.type === 'material' ? 'slider right' : 'slider'}
          ></li>
        </NavUl>
      </nav>
      <Search
        outerbg="#fff"
        innerbg="#efefef"
        hasborder={false}
      ></Search>
      <Switch>
        <Redirect from={url} to={`${url}/category`} exact></Redirect>
        <Route path={`${url}/category`}>
          <Category {...props }></Category>
        </Route>
        <Route path={`${url}/material`}>
          <Material {...props}></Material>
        </Route>
      </Switch>
    </CategoryWrap>
  )
}
