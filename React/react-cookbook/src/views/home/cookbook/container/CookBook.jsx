import React, { Component } from 'react'
import BScroll from 'better-scroll'
import { connect } from 'react-redux'
import CookBookUi from '../ui/CookBookUi'
import { loadDataSyncSaga } from '../actionCreator'
import { httpContext } from '@/context/httpContext'

@connect(
  (state) => ({
    list: state.cookbook.list
  }),
  disaptch => ({
    loadData() {
      disaptch(loadDataSyncSaga())
    }
  })
)
class CookBook extends Component {
  static contextType = httpContext

  state = {
    hotcate: []
  }

  render() {
    return (
      <CookBookUi 
        {...this.props} 
        hotcate={this.state.hotcate}
      ></CookBookUi>
    )
  }

  async componentDidMount() {
    // 拉取list数据
    this.props.loadData()

    // 拉取hotcate数据
    let hotcate = await this.context.http.get({
      url: '/api/hotcat'
    })
    hotcate.push({
      title: '更多...'
    })
    this.setState({
      hotcate
    }, () => {
      // 区域滚动
      this.bscroll = new BScroll('#cookbook-scroll')
    })
  }

  componentDidUpdate() {
    // 当列表数据更新的时候，让scroll根据新的高度重新计算
    this.bscroll && this.bscroll.refresh()
  }
}

export default CookBook
