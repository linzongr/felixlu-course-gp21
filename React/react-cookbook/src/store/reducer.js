import { combineReducers } from 'redux'

import { reducer as cookbook } from '@/views/home/cookbook'
import { reducer as more } from '@/views/home/more'
const reducer = combineReducers({
  cookbook,
  more
})

export default reducer