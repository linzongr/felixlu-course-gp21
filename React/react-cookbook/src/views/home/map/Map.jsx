import React, { Component } from 'react'
import { MapWrap } from './StyledMap'

export default class Map extends Component {
  render() {
    return (
      <MapWrap>
        <iframe src="/map.html" title="map" frameBorder="0"></iframe>
      </MapWrap>
    )
  }
}
