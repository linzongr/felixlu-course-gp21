var express = require('express');
var router = express.Router();
var { graphql } = require('graphql')
var schema = require('../MyGraphQLSchema')

/* GET home page. */
router.get('/list', function(req, res, next) {
  var query = `
    {
      hello,
      movies {
        title
      }
    }
  `

  graphql(schema, query).then(result => {
    res.json(result)
  })
});

module.exports = router;