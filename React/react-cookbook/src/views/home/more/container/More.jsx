import React, { Component } from 'react'
import { connect } from 'react-redux'
import MoreUi from '../ui/MoreUi'
import { changeSaga } from '../actionCreator'

@connect(
  state => ({
    isShowMap: state.more.isShowMap
  }),
  dispatch => ({
    handleChangeShowMap(isShowMap) {
      dispatch(changeSaga(isShowMap))
    }
  })
)
class More extends Component {
  // state = {
  //   isShowMap: true
  // }

  // handleChangeShowMap = checked => {
  //   this.setState({
  //     isShowMap: checked
  //   })
  // }

  render() {
    return (
      <MoreUi {...this.props}></MoreUi>
    )
  }
}

export default More
